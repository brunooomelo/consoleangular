var gulp			= require('gulp'),
	sass 			= require('gulp-sass'),
	concat			= require('gulp-concat'),
	uglify			= require('gulp-uglify'),
	imagemin		= require('gulp-imagemin'),
	mincss			= require('gulp-minify-css'),
	browsersync		= require('browser-sync'),
	gulpSeq 		= require('gulp-sequence').use(gulp),
	shell 			= require('gulp-shell'),
	babel 			= require('gulp-babel'),
	plumber			= require('gulp-plumber');

// IN DEVELOPMENT

gulp.task('html', function() {
       return gulp.src('./src/**/*.html')
        .pipe(plumber())
        .pipe(browsersync.reload({stream: true}))
});

gulp.task('scss', function(){
	gulp.src('./src/public/scss/**/*.scss')
		.pipe(plumber())
		.pipe(sass())
		.pipe(gulp.dest('./public/src/css/'))
		.pipe(browsersync.reload({stream: true}))
});

gulp.task('viewbrowser', function() {
     browsersync.init({
        server: {
            baseDir: "./src/"
        }
    });
});

// gulp.task('ES6',function(){
//     return gulp.src('./console/src/js/*.js')
//         .pipe(babel({
//             presets: ['es2015']
//         }))
//         .pipe(gulp.dest('src/js/'));
// });
// CLEAN FOLDER

gulp.task('clean', function() {
    return shell.task([
      'rm -rf dist'
    ]);
});

// CREATE FOLDERS
gulp.task('scaffold', function() {
  return shell.task([
      'mkdir dist',
      'mkdir dist/fonts',
      'mkdir dist/images',
      'mkdir dist/scripts',
      'mkdir dist/styles'
    ]
  );
});


// IN DEPLOY

gulp.task('build-js', function(){
  return gulp.src('./src/js/*.js')
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/scripts/'));
});

gulp.task('build-js-es6', function(){
	return gulp.src('./src/js/*.js')
		.pipe(plumber())
    .pipe(babel({
      presets: ['es2015']
    }))
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dist/scripts/'));
});

gulp.task('build-css', function(){
	 gulp.src('./src/css/*.css')
		.pipe(plumber())
		.pipe(concat('main.css'))
		.pipe(mincss())
		.pipe(gulp.dest('./dist/styles/'));
});

gulp.task('build-img', function(){
	gulp.src('./src/img/*')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('build-html', function(){
	 gulp.src('./src/*.html')
		.pipe(plumber())
		.pipe(gulp.dest('./dist/'));
});


// IN DEV

gulp.task('sone', ['viewbrowser', 'html'], function(){
  gulp.watch('./src/js/**/*.js', ['html']);
	gulp.watch('./src/**/*.html', ['html']);
});

gulp.task('ES6', ['viewbrowser', 'scss', 'html'], function(){
	gulp.watch('./src/scss/**/*.scss', ['scss']);
	gulp.watch('./src/js/**/*.js', ['ES6']);
	gulp.watch('src/**/*.html', ['html']);
});
// DEPLOY
gulp.task('deploy', gulpSeq('clean', 'scaffold', ['build-js', 'build-css', 'build-img'],'build-html'));
gulp.task('deploy-es6', gulpSeq('clean', 'scaffold', ['build-js-es6', 'build-css', 'build-img'],'build-html'));
