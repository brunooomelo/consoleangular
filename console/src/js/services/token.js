angular
  .module('backoffice')
  .service('tokenService', TokenService)

  function TokenService($http, config, uoletContext){
    this.UoletContext = uoletContext
    this.xhr = $http 
    this.config = config
  }

  TokenService.prototype.Login = function(credentials){
    //TOFIX: mover para alguma inicializacao global
    this.xhr.defaults.headers.common['x-client-id'] = this.config.clientId;
    var self = this;
    return this.xhr({
              method: 'POST',
              url: this.config.apiEndpoint + '/token',
              data: {
                subject: credentials.subject,
                secret: credentials.secret,
                merchant_context: credentials.merchant_context,
                type: 'uolet',
                grant_type: 'password'
              }
            })
            .then(function(result){
              self.xhr.defaults.headers.common['Authorization'] = 'Bearer ' + result.data.access_token;
              sessionStorage.setItem('uolet:console:token', result.data.access_token)
              return result 
            })
            .catch(tratamentoError)
            .then(function(result){
              return self.getContext()
            })
  }

  TokenService.prototype.getContext = function(){
    var self = this;
    return this.xhr({
            'method': 'GET',
            'url': this.config.apiEndpoint + '/context',
          })  
        .then(function(result){
          self.UoletContext.data = result.data
          return result
        })
        .catch(tratamentoError)
  }

  TokenService.prototype.Logout = function(){
    return new Promise(function(resolve){
      this.xhr.defaults.headers.common['Authorization'] = null
      sessionStorage.removeItem('uolet:application:token')
      sessionStorage.removeItem('uolet:console:token')

      return resolve(true)
    })
  }
  TokenService.prototype.trocaMerchant = function(credentials) {
    var self = this
    return this.xhr({
              'method': 'POST',
              'url': this.config.apiEndpoint + '/token',
              'data': {
                merchant_context: credentials.merchant_context,
                grant_type: 'refresh_token'
              },
              headers: {
                'Authorization': 'Bearer '+sessionStorage.getItem('uolet:console:token')
              }
            })
            .then(function(result){
              self.xhr.defaults.headers.common['Authorization'] = 'Bearer ' + result.data.access_token;
              sessionStorage.setItem('uolet:console:token', result.data.access_token)
              return result
            })
            .catch(tratamentoError)
            .then(function(result){
              return self.getContext()
            })
  };
  TokenService.prototype.TrocaApp = function(idApp) {
    var self = this
    return this.xhr({
      method: 'POST',
      url: self.config.apiEndpoint + '/token',
      data: {
        'target_id': idApp,
        'grant_type': 'refresh_token',
        'refresh_token': sessionStorage.getItem('uolet:console:token') || self.xhr.defaults.headers.common['Authorization']
      }
    })
    .then(function(result){
      sessionStorage.setItem('uolet:application:token', result.data.access_token)
      return result
    })
    .catch(tratamentoError)
    .then(function(result){
      return self.getContext()
    })
  };
TokenService.prototype.TrocaAppMerchant = function(credentials) {
  var self = this
  return this.xhr({
            'method': 'POST',
            'url': this.config.apiEndpoint + '/token',
            'data': {
              merchant_context: credentials.merchant_context,
              grant_type: 'refresh_token'
            }
          })
          .then(function(result){
            self.xhr.defaults.headers.common['Authorization'] = 'Bearer ' + result.data.access_token;
            sessionStorage.setItem('uolet:console:token', result.data.access_token)
            return result
          })
          .catch(tratamentoError)
          .then(function(result){
            return self.getContext()
          })
  };

 function tratamentoError(error) {
        if(error.status >= 400 && error.status < 500){
          if(error.statusText == "Not Found" || error.statusText == "Unauthorized"){
            throw new Error("Usuario ou Senha estão incorretos!")
          }
          if(error.statusText == "Bad Request"){
          //client  
            if(error.data.BadMethod == "missing required authorization token."){
              throw new Error("Token ausente, por favor faça o login novamente!")
            }
            if(error.data.BadMethod == "token does not belong to client id .") {
              throw new Error('Token inválido, por favor faça o login novamente!')
            }
            if(error.data.BadMethod == "missing required client_id."){
              throw new Error("ClientId não enviado, Por favor contate o Suporte")
            } 

            // merchant 
            if(error.data.BadMethod == "invalid merchant context."){
              throw new Error("Merchant invalido, Por favor inserir um merchant valido.")
            } 
            if(error.data.BadMethod == "user is not authorized by merchant."){
              throw new Error("Você não tem acesso para utilizar em nome deste merchant")
            }
            else{
              throw new Error("Sua requisição com a aplicação falhou, por favor faça o login novamente!")
            }
          }
        }
        if(error.status >= 500 ) {
          throw new Error('Sem conexão com o serviço Uolet!')
        }
        if(error.data.access_token == "sem_acesso") {
          throw new Error('Sem acesso')
        }
        throw new Error('Erro desconhecido.')
  }
  