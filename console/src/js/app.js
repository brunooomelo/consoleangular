angular
  .module('backoffice', ['ngRoute', 'angular-md5', 'oitozero.ngSweetAlert'])
  .constant('config', {
      "clientId": "5898cfc4298ead775655decb",
      "apiEndpoint": "http://localhost:8080",
      "apps": [
          {
              "name": "Gerenciador de Cliente",
              "categoria": "Gerenciador",
              "url_app": "https://www.youtube.com/embed/TL1ByAIf8Ck?list=PLtHrsFFj3lWFFopR-dRS6VCeOP6PWo3JO",
              "client_id":"1",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"red"
          },
          {
              "name": "Gerenciador de Wifi",
              "categoria": "Gerenciador",
              "url_app": "https://www.youtube.com/embed/76E3huoplZ4?list=PLtHrsFFj3lWFFopR-dRS6VCeOP6PWo3JO",
              "client_id":"2",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"yellow"
          },
          {
              "name": "Gerenciador de Merchant",
              "categoria": "Gerenciador",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"3",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"brown"
          },
          {
              "name": "Gerenciador de Usuarios",
              "categoria": "Gerenciador",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"4",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"blackandwhite"
          },
          {
              "name": "Relatorios de Usuarios",
              "categoria": "Relatorios",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"5",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"black"
          },
          {
              "name": "Relatorios de Wifi",
              "categoria": "Relatorios",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"6",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"green"
          },
          {
              "name": "Relatorios de Apps",
              "categoria": "Relatorios",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"7",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"gray"
          },
          {
              "name": "Relatorios de Compras",
              "categoria": "Relatorios",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"8",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"white"
          },
          {
              "name": "Editor de Console",
              "categoria": "Campanhas",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"9",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"pink"
          },
          {
              "name": "Editor de Console II ",
              "categoria": "Campanhas",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"10",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"blue"
          },
          {
              "name": "Editor de Usuario",
              "categoria": "Campanhas",
              "url_app": "https://www.youtube.com/embed/Yt3DXAzg1FE",
              "client_id":"11",
              "url_icon":"./public/img/task-done-flat.png",
              "color":"purple"
          }
      ]
    })
  .value('uoletContext', {data: null, currentApp: {urlApp: null, idApp: null}})
  .config(['config', '$routeProvider' ,function(config ,$routeProvider){
    if(localStorage.getItem('uolet_console:configApps')){
      config = Object.assign(config, JSON.parse(localStorage.getItem('uolet_console:configApps')), config)
    }
    sessionStorage.removeItem('uolet:application:token')
    sessionStorage.removeItem('uolet:console:token')
    $routeProvider
      .when('/', {
        templateUrl: 'view/pages/login.html',
        controller: 'loginController'
       })
      .when('/home', {
        templateUrl: 'view/pages/home.html',
      })
      .otherwise({
        redirectTo:'/'
      });
      window.location.href="#!/"    
  }])