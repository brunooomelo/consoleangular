angular
  .module('backoffice')
  .controller('loginController', LoginController);

  function LoginController(tokenService, md5, SweetAlert, uoletContext){
    this.tokenService = tokenService
    this.md5 = md5
    this.SweetAlert = SweetAlert
    this.uoletContext = uoletContext
  }

  LoginController.prototype.login = function(usuario) {
    var that = this
    var credentials = {
                'subject': usuario.subject,
                'secret': this.md5.createHash(usuario.secret),
                'merchant_context': usuario.merchant_context.toUpperCase()
            }
            swal({
                'title': 'Carregando',
                'imageUrl': './public/gif/loading.gif',
                'showConfirmButton': false
            })
            this.tokenService.Login(credentials)
                .then(function(data){
                    swal({
                        'title': 'Bem vindo a console Uolet',
                        'text': that.uoletContext.data.user.fullname + ', Você entrou em nome do merchant: ' + that.uoletContext.data.merchant.name,
                        'timer': 2000,
                        'type': 'success'
                    }, function(){
                        window.location.href = '#!/home'
                        swal.close()
                    });
                })
                .catch(function(error){
                    swal({
                        'title': 'Ocorreu um erro',
                        'text': error.message,
                        'timer': 2000,
                        'type': 'error',
                        'showConfirmButton': false
                    })
                })
  };