angular
  .module('backoffice')
  .controller('homeController', HomeController)
  .filter('trustedUrl', trustedUrl);

  function trustedUrl($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url)
    }
  }
  function HomeController(tokenService, SweetAlert, uoletContext, config){
    this.tokenService = tokenService
    this.SweetAlert = SweetAlert
    this.UoletContext = uoletContext
    this.config = config
  }

  HomeController.prototype.logout = function() {
    var self = this;
    swal({
        title: "Certeza que deseja Sair?",
        text: "Você não terá mais acesso aos aplicativos!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, Desejo sair!",
        closeOnConfirm: false
      },
      function(){
        swal({
          title: "Logout com Sucesso",
          text: "Obrigado por utilizar nosso Sistema Uolet !",
          type: "success",
          timer: 1000
        }, function() {
          self.tokenService.Logout()
          window.location.href= '/'
        });
      });
  };
  HomeController.prototype.trocaMerchant = function(credentials) {
    var self = this;
    console.log
    if(this.UoletContext.currentApp.urlApp !== null){
      return this.tokenService.TrocaAppMerchant(credentials)
                .then(function(result){
                  return result
                })
                .then(function(result){
                  self.TrocaApp(self.UoletContext.currentApp.idApp, self.UoletContext.currentApp.urlApp)
                  document.getElementById('iframe').contentWindow.location.replace(self.UoletContext.currentApp.urlApp)
                })
                .catch(function(error){
                  swal({
                      'title': 'Ocorreu um erro',
                      'text': error.message,
                      'timer': 2000,
                      'type': 'error',
                      'showConfirmButton': false
                  })
                  sessionStorage.removeItem('uolet:application:token')
                  self.Home()
                })
    }
    this.tokenService.trocaMerchant(credentials)
      .then(function(result){
        swal({
            'title': 'Bem vindo a console Uolet',
            'text': self.UoletContext.data.user.fullname + ', Você entrou em nome do merchant: ' + self.UoletContext.data.merchant.name,
            'timer': 2000,
            'type': 'success'
        }, function(){
            swal.close()
            window.location.href = '#!/home'
        });
      })
      .catch(function(error){
        swal({
            'title': 'Ocorreu um erro',
            'text': error.message,
            'timer': 2000,
            'type': 'error',
            'showConfirmButton': false
        })
      })

  }
  HomeController.prototype.TrocaApp = function(idApp, urlApp) {
    var self = this
    this.tokenService.TrocaApp(idApp)
      .then(function(result){
         self.UoletContext.currentApp = {
          idApp : idApp,
          urlApp: urlApp
        }
      })
      .catch(function(error){
        swal({
            'title': 'Ocorreu um erro',
            'text': error.message,
            'timer': 2000,
            'type': 'error',
            'showConfirmButton': false
        })
      })
  };
  HomeController.prototype.Home = function() {
    this.UoletContext.currentApp.urlApp = null
  };

  HomeController.prototype.EditConfig = function(submit) {
    var self = this
    try {
      swal({
            'title': 'Alterado com Sucesso!',
            'timer': 800,
            'type': 'success',
            'showConfirmButton': false
        }, function(){
            swal.close()
            $('#settingsModal').modal('hide')
            localStorage.setItem('uolet_console:configApps', JSON.stringify(Object.assign(self.config, submit)))
        })
    } catch(err){
      throw new Error(err)
    }
  };
  HomeController.prototype.AddApps = function(apps) {
      var submit = this.config.apps.push(angular.copy(apps))
      var self = this
      try {
        swal({
              'title': 'Alterado com Sucesso!',
              'timer': 800,
              'type': 'success',
              'showConfirmButton': false
          }, function(){
              swal.close()
              $('#cadastroApp').hide()
              localStorage.setItem('uolet_console:configApps', JSON.stringify(Object.assign(self.config, submit)))
          })
      } catch(err){
        throw new Error(err)
      }
  };
  HomeController.prototype.DeleteApps = function(first_argument) {

  };
