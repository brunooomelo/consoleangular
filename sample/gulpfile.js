var gulp      = require('gulp'),
  sass      = require('gulp-sass'),
  concat      = require('gulp-concat'),
  uglify      = require('gulp-uglify'),
  imagemin    = require('gulp-imagemin'),
  mincss      = require('gulp-minify-css'),
  browsersync = require('browser-sync'),
  gulpSeq     = require('gulp-sequence').use(gulp),
  shell       = require('gulp-shell'),
  babel       = require('gulp-babel'),
  plumber   = require('gulp-plumber');

// IN DEVELOPMENT

gulp.task('html', function() {
       return gulp.src('src/**/*.html')
        .pipe(plumber())
        .pipe(browsersync.reload({stream: true}))
});

gulp.task('scss', function(){
  gulp.src('./src/public/scss/**/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('./src/public/css/'))
    .pipe(browsersync.reload({stream: true}))
});

gulp.task('viewbrowser', function() {
     browsersync.init({
        server: {
            baseDir: "./src/"
        }
    });
});
// CLEAN FOLDER

gulp.task('clean', function() {
    return shell.task([
      'rm -Rf dist'
    ]);
});

// CREATE FOLDERS
gulp.task('scaffold', function() {
  return shell.task([
      'mkdir dist',
      'mkdir dist/fonts',
      'mkdir dist/images',
      'mkdir dist/scripts',
      'mkdir dist/styles'
    ]
  );
});


// IN DEPLOY

gulp.task('build-js', function(){
  return gulp.src('./src/public/js/*.js')
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/public/js'));
});

gulp.task('build-css', function(){
   gulp.src('./src/public/css/*.css')
    .pipe(plumber())
    .pipe(concat('main.css'))
    .pipe(mincss())
    .pipe(gulp.dest('./dist/public/css'));
});

gulp.task('build-img', function(){
  gulp.src('./src/public/img/*')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('build-html', function(){
   gulp.src('./src/*.html')
    .pipe(plumber())
    .pipe(gulp.dest('./dist'));
});


// IN DEV

gulp.task('default', ['viewbrowser', 'scss', 'html'], function(){
  gulp.watch('./src/public/scss/**/*.scss', ['scss']);
  gulp.watch('./src/public/js/**/*.js', ['html']);
  gulp.watch('./src/**/*.html', ['html']);
});

// DEPLOY
gulp.task('deploy', gulpSeq('clean', 'scaffold', ['build-js', 'build-css', 'build-img'],'build-html'));

